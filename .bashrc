# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace
# Ignore duplicates, ls without options and builtin commands
export HISTIGNORE="&:ls:[bf]g:exit"

# append to the history file, don't overwrite it
shopt -s histappend
# Combine multiline commands into one in history
shopt -s cmdhist

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
#History can be very useful, but by default on most distributions your history is
# blown away by each shell exiting, and it doesn't hold much to begin with. I like to have 10,000 lines of history:
export HISTFILESIZE=20000
export HISTSIZE=10000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

#use extra globing features. See man bash, search extglob.
# shopt -s extglob
#include .files when globbing.
shopt -s dotglob
#When a glob expands to nothing, make it an empty string instead of the literal characters.
# shopt -s nullglob
# fix spelling errors for cd, only in interactive shell
shopt -s cdspell

### show colnum file
function col {
    first="awk '{print "
    last="}' $2"
    cmd="${first}\$${1}${last}"
    echo $cmd
    eval $cmd
}

### key add
function keysend {
    if [ -f ~/.ssh/id_rsa.pub ]; then
	first="cat ~/.ssh/id_rsa.pub | ssh "
    else
	if [ -f ~/.ssh/id_dsa.pub ]; then
	    first="cat ~/.ssh/id_dsa.pub | ssh "
	fi
    fi
    #second=" 'if ![-d .ssh] mkdir .ssh fi; cat >> .ssh/authorized_keys' "
    second=" 'cat >> .ssh/authorized_keys' "
    cmd="${first}${1}${second}"
    echo $cmd
    eval $cmd
}

### key add
function keysend2 {
    if [ -f ~/.ssh/id_rsa.pub ]; then
	first="cat ~/.ssh/id_rsa.pub | ssh "
    else
	if [ -f ~/.ssh/id_dsa.pub ]; then
	    first="cat ~/.ssh/id_dsa.pub | ssh "
	fi
    fi

    second=" 'mkdir -p .ssh; cat >> .ssh/authorized_keys' "
    #second=" 'cat >> .ssh/authorized_keys' "
    cmd="${first}${1}${second}"
    echo $cmd
    eval $cmd
}


#################################################################
# extract files. Ignore files with improper extensions.
extract () {
    local x
    ee() { # echo and execute
	echo "$@"
	$1 "$2"
    }
    for x in "$@"; do
	[[ -f $x ]] || continue
	case "$x" in
	    *.tar.bz2 | *.tbz2 )ee "tar xvjf" "$x";;
	    *.tar.gz | *.tgz ) ee "tar xvzf" "$x";;
	    *.bz2 )ee "bunzip2" "$x";;
	    *.rar )ee "unrar x" "$x"   ;;
	    *.gz )ee "gunzip" "$x"    ;;
	    *.tar )ee "tar xvf" "$x"   ;;
	    *.zip )ee "unzip" "$x"     ;;
	    *.Z )ee "uncompress" "$x" ;;
	    *.7z )ee "7z x" "$x"      ;;
	esac
    done
}
#################################################################

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

### Host aliases
if [ -f ~/.bash_host_aliases ]; then
    . ~/.bash_host_aliases
fi

### VirtualEnv
## export WORKON_HOME=$HOME/.virtualenvs
## source /opt/local/Library/Frameworks/Python.framework/Versions/Current/bin/virtualenvwrapper.sh
## export PIP_VIRTUALENV_BASE=$WORKON_HOME


### add https://github.com/rupa/z so we can easily change directories
. ~/.z.sh


##############################################################################
## Do all path setting here
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
export PATH=/opt/local/bin:/opt/local/sbin:/usr/local/share/npm/bin:$PATH

## IF DEF MAC
# Finished adapting your PATH environment variable for use with MacPorts.
if [ "$(uname)" == "Darwin" ]; then
    export PATH=/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin:$PATH
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # added by Miniconda3 3.7.3 installer for Python 3.x
    # export PATH="/opt/miniconda3/bin:$PATH"
    # added by Miniconda 3.7.3 installer for Python 2.x
    #export PATH="/opt/miniconda/bin:$PATH"
    export NOCONDAPATH=$PATH
    export CONDAPATH="/opt/miniconda/bin:$PATH"
fi

function condapath {

    export PATH=$CONDAPATH
}

function origpath {
    export PATH=$NOCONDAPATH
}
