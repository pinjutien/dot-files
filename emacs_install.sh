apt-get install -y git-core libxaw7-dev libxpm-dev libpng12-dev libtiff5-dev libgif-dev libjpeg8-dev libgtk2.0-dev libncurses5-dev autoconf automake
apt-get build-dep emacs
git clone --depth 1 -b master git://git.sv.gnu.org/emacs.git
cd emacs
./autogen.sh
./configure --prefix=/opt/emacs25
make
sudo make install
