
### Git stuff
alias ga='git add'
alias gp='git push'
alias gl='git log'
alias gs='git status'
alias gd='git diff'
alias gm='git commit -m'
alias gma='git commit -am'
alias gb='git branch'
alias gc='git checkout'
alias gra='git remote add'
alias grr='git remote rm'
alias gpu='git pull'
alias gcl='git clone'



### Other useful stuff from: http://serverfault.com/questions/5249/useful-bash-aliases-and-generating-a-listing-of-your-most-used-commands

## dir traversal
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias cd..='cd ..'

## typos
alias ll="ls -l"
alias lo="ls -o"alias lh="ls -lh"
alias la="ls -la"
alias sl="ls"
alias l="ls"
alias s="ls"
alias lt='ls -lart'

## egrep instead of grep
alias grep="egrep"

## all man pages
alias man="man -a"

## net tools
alias pg='ping 8.8.8.8'


# reboot / halt / poweroff
alias reboot='sudo /sbin/reboot'
alias poweroff='sudo /sbin/poweroff'
alias halt='sudo /sbin/halt'
alias shutdown='sudo /sbin/shutdown'

## clean
alias clean="rm -f *~ *#"

## play video files in a current directory ##
# cd ~/Download/movie-name
# playavi or vlc
alias playavi='mplayer *.avi'
alias vlc='vlc *.avi'

# play all music files from the current directory #
alias playwave='for i in *.wav; do mplayer "$i"; done'
alias playogg='for i in *.ogg; do mplayer "$i"; done'
alias playmp3='for i in *.mp3; do mplayer "$i"; done'

# play files from nas devices #
alias nplaywave='for i in /nas/multimedia/wave/*.wav; do mplayer "$i"; done'
alias nplayogg='for i in /nas/multimedia/ogg/*.ogg; do mplayer "$i"; done'
alias nplaymp3='for i in /nas/multimedia/mp3/*.mp3; do mplayer "$i"; done'

# shuffle mp3/ogg etc by default #
alias music='mplayer --shuffle *'


alias ew='emacs -nw $i'

alias pyinstall="sudo python setup.py install --record files.txt"
alias pyuninstall="cat files.txt | xargs sudo rm -rf"

alias py=python
alias mkuser=/usr/sbin/adduser

function mig() {
    python manage.py schemamigration "$@" --auto;
    python manage.py migrate "$@";
}

alias ed="emacs25 --daemon &"
alias ec="emacsclient25 ."
alias ec25="emacsclient25"
alias s1="ssh rbansal@h1"
alias s2="ssh rbansal@h2"
alias path='readlink -e'
