(set 'package-enable-at-startup 1)
(package-initialize)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Visual bell

(put 'upcase-region 'disabled nil)
;(tool-bar-mode -1)                             ; No toolbar
;(menu-bar-mode -1)                             ; No menubar
;(scroll-bar-mode -1)                           ; No scrollbar
;(set-scroll-bar-mode 'right)                   ; Scrollbar on the right
;(setq inhibit-startup-message t)               ; No message at startup
;(setq visible-bell t)                          ; No beep when reporting errors
;(global-font-lock-mode 1)                      ; Color enabled



;; Set to the location of your Org files on your local system
;; (setq org-directory "~/org")
;; Set to <your Dropbox root directory>/MobileOrg.
;; (setq org-directory "~/Dropbox/org")
;; (setq org-mobile-directory "~/Dropbox/MobileOrg")
;; Set to the location of your Org files on your local system
;; (setq org-agenda-files '("~/Dropbox/org/todo.org" "~/Dropbox/org/js.org"))
;; (setq org-mobile-inbox-for-pull "~/Dropbox/org/from-mobile.org")
;;(setq org-mobile-inbox-for-pull "~/org/flagged.org")


;; Turn off mouse interface early in startup to avoid momentary display
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
;(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; No splash screen please ... jeez
(setq inhibit-startup-message t)

;; Set path to dependencies
;(setq site-lisp-dir
;      (expand-file-name "site-lisp" user-emacs-directory))

;; Set up load path
;(add-to-list 'load-path user-emacs-directory)
(add-to-list 'load-path      (concat user-emacs-directory "lisp/"))
(add-to-list 'load-path      (concat user-emacs-directory "elpa/"))
;(add-to-list 'load-path site-lisp-dir)

;; Settings for currently logged in user
(setq user-settings-dir
      (concat user-emacs-directory "users/" user-login-name))
(add-to-list 'load-path user-settings-dir)

;; Add external projects to load path
;(dolist (project (directory-files site-lisp-dir t "\\w+"))
;  (when (file-directory-p project)
;    (add-to-list 'load-path project)))

;; Keep emacs Custom-settings in separate file
;(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
;(load custom-file)

;; Write backup files to own directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name
                 (concat user-emacs-directory "backups")))))

;; Make backups of files, even when they're in version control
(setq vc-make-backup-files t)

;; Save point position between sessions
(require 'saveplace)
(require 'let-alist)
(setq-default save-place t)
(setq save-place-file (expand-file-name ".places" user-emacs-directory))

;; Are we on a mac?
(setq is-mac (equal system-type 'darwin))

;; Setup packages
(require 'setup-package)
;;(require 'let-alist-1.0.1)

;; Install extensions if they're missing
(defun init--install-packages ()
  (packages-install
   ;;   (cons 'sx melpa)
   (cons 'paredit melpa)
   (cons 'flymake-cursor melpa)
   (cons 'buster-snippets melpa)

   (cons 'shell-command melpa)
   (cons 'diminish melpa)
   (cons 'zoom-frm melpa)
   (cons 'elpy melpa)
   (cons 'multiple-cursors melpa)
   (cons 'exec-path-from-shell melpa)
   (cons 'dired-details melpa)
   (cons 'magit melpa)
;   (cons 'paredit melpa)
                                        ;   (cons 'move-text melpa)
   (cons 'smooth-scrolling melpa)
   (cons 'gist melpa)
   (cons 'fill-column-indicator melpa)
   (cons 'htmlize melpa)
   (cons 'smartparens melpa)
   (cons 'elisp-slime-nav melpa)
   (cons 'smex melpa)
   (cons 'ido melpa)
   (cons 'ido-ubiquitous melpa)
;   (cons 'yasnippet melpa)
;   (cons 'perspective melpa)

   (cons 'find-file-in-project melpa)
;   (cons 'nrepl melpa)
;   (cons 'expand-region melpa)
;   (cons 'ess melpa)
   (cons 'auto-complete melpa)
   (cons 'color-theme melpa)
;   (cons 'flycheck melpa)
   (cons 's melpa) ;; magnars string mainupulation library https://github.com/magnars/s.el
   ;(cons 'ox melpa) ;; freemind export stuff ugh
))

(condition-case nil
    (init--install-packages)
  (error
   (package-refresh-contents)
   (init--install-packages)))


(package-initialize)


;; Lets start with a smattering of sanity
(require 'sane-defaults)

;; Setup environment variables from the user's shell.
(when is-mac (exec-path-from-shell-initialize))

;; Setup extensions
(eval-after-load 'ido '(require 'setup-ido))
(eval-after-load 'org '(require 'setup-org))
(eval-after-load 'dired '(require 'setup-dired))
;(eval-after-load 'magit '(require 'setup-magit))
(eval-after-load 'grep '(require 'setup-rgrep))
(eval-after-load 'shell '(require 'setup-shell))
(require 'setup-hippie)
;(require 'setup-yasnippet)
;(require 'setup-perspective)
(require 'setup-ffip)
(require 'setup-html-mode)
;(require 'mac)

;(require 'setup-paredit)

;; Default setup of smartparens
(require 'smartparens-config)

;; Language specific setup files
;(eval-after-load 'js2-mode '(require 'setup-js2-mode))
;(eval-after-load 'ruby-mode '(require 'setup-ruby-mode))
(eval-after-load 'markdown-mode '(require 'setup-markdown-mode))

;; Slime-js uncomment
;; Load slime-js when asked for
;(autoload 'slime-js-jack-in-browser "setup-slime-js" nil t)
;(autoload 'slime-js-jack-in-node "setup-slime-js" nil t)

;; Map files to modes
(require 'mode-mappings)

;; Functions (load all files in defuns-dir)
(setq defuns-dir (expand-file-name "defuns" user-emacs-directory))
(dolist (file (directory-files defuns-dir t "\\w+"))
  (when (file-regular-p file)
    (load file)))

;;(require 'expand-region)
;;(require 'mark-more-like-this)
;;(require 'inline-string-rectangle)
(require 'multiple-cursors)
;(require 'delsel)
;;(require 'jump-char)
;; (require 'eproject)
;;(require 'wgrep)
;;(require 'smart-forward)
;(require 'change-inner)
;;(require 'multifiles)

;; Fill column indicator
(require 'fill-column-indicator)
(setq fci-rule-color "#111122")

;; Browse kill ring
;;(require 'browse-kill-ring)
;;(setq browse-kill-ring-quit-action 'save-and-restore)

;; Smart M-x is smart
(require 'smex)
(smex-initialize)

;; Setup key bindings
(require 'key-bindings)

;(require 'ess)

;; Auto complete
;(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(require 'auto-complete-config)
(ac-config-default)

;; Misc
(require 'appearance)
(require 'my-misc)
(when is-mac (require 'mac))

;; Diminish modeline clutter
(require 'diminish)
;(diminish 'yas/minor-mode)
(diminish 'eldoc-mode)
;(diminish 'paredit-mode)

;; Elisp go-to-definition with M-. and back again with M-,
;; Slime-js uncomment
;(autoload 'elisp-slime-nav-mode "elisp-slime-nav")
;(add-hook 'emacs-lisp-mode-hook (lambda () (elisp-slime-nav-mode t) (eldoc-mode 1)))
;(eval-after-load 'elisp-slime-nav '(diminish 'elisp-slime-nav-mode))

;; Email, baby
;; (require 'setup-mu4e)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; elpy
;; (elpy-enable)

(when (require 'elpy nil t)
  ;; Completion backend
  (setq elpy-rpc-backend "jedi")
  (elpy-enable)

  ;; pops up documentation next to company-quickhelp
  ;; (company-quickhelp-mode 1)

  ;(elpy-clean-modeline)
)



;; Emacs server
;; (require 'server)
;; (unless (server-running-p)
;; (server-start))

;; Run at full power please
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

;; Conclude init by setting up specifics for the current user
(when (file-exists-p user-settings-dir)
  (mapc 'load (directory-files user-settings-dir nil "^[^#].*el$")))

;(require 'ox)
;(require 'ox-html)
;(require 'ox-org)
;(load "~/ox-freemind.el")

;; key bindings
;(when (eq system-type 'darwin) ;; mac specific settings
;  (setq mac-option-modifier 'meta
;
;)

(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region
Equivalent to \\[set-mark-command] when \\[transient-mark-mode] is disabled"
  (interactive)
  (push-mark (point) t nil)
  (message "Pushed mark to ring")
)

(global-set-key (kbd "C-x '") 'push-mark-no-activate)

;)

;;; color theme
;(require 'color-theme)
;(color-theme-initialize)
;(color-theme-charcoal-black)



(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)

;(when (string= system-name "housing.rationalinsights.com")
;  (setq make-backup-files nil)
;)


;;; switch to spaces
;;; and set tab width to 4
(setq indent-tabs-mode nil)
(setq-default indent-tabs-mode nil)
(add-hook 'python-mode-hook
      (lambda ()
        (setq indent-tabs-mode nil)
        (setq-default indent-tabs-mode nil)
        (setq tab-width 4)
        (setq python-indent 4)))


;; In an interactive search, a space character stands for one or more whitespace characters
;; (tabs are whitespace characters). You can use M-s SPC while searching or replacing to
;; toggle between this behavior and treating spaces as literal spaces. Or put the following in your InitFile to override this behaviour.
(setq search-whitespace-regexp nil)


;; (when (load "flymake" t)
;;        (defun flymake-pylint-init ()
;;          (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                             'flymake-create-temp-inplace))
;;                (local-file (file-relative-name
;;                              temp-file
;;                            (file-name-directory buffer-file-name))))
;;            (list "epylint" (list local-file))))
;;       (add-to-list 'flymake-allowed-file-name-masks
;;                     '("\\.py\\'" flymake-pylint-init)))


;(setq elpy-default-minor-modes
;        (remove 'flymake-mode
;                elpy-default-minor-modes))


(set-face-foreground 'minibuffer-prompt "green")
(setenv "PYTHONPATH" "./library")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes nil)
 '(dired-listing-switches "-altr")
 '(package-selected-packages
   (quote
    (bash-completion zoom-frm smooth-scrolling smex smartparens shell-command s paredit multiple-cursors magit ido-ubiquitous htmlize gist flymake-cursor fill-column-indicator exec-path-from-shell elpy elisp-slime-nav dired-details diminish color-theme buster-snippets auto-complete))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(isearch-fail ((t (:background "color-56"))))
 '(magit-section-highlight ((t (:background "color-25")))))

(add-to-list 'yas-snippet-dirs "~/.emacs.d/yasnippet-snippets")
(yas-initialize)



(setq pycodechecker "~/bin/pylint_etc_wrapper.py")
(when (load "flymake" t)
  (load-library "flymake-cursor")
  (defun dss/flymake-pycodecheck-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list pycodechecker (list local-file))))
  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.py\\'" dss/flymake-pycodecheck-init)))

;; And here are two little helpers for quickly silencing a warning message:

(defun dss/pylint-msgid-at-point ()
  (interactive)
  (let (msgid
        (line-no (line-number-at-pos)))
    (dolist (elem flymake-err-info msgid)
      (if (eq (car elem) line-no)
            (let ((err (car (second elem))))
              (setq msgid (second (split-string (flymake-ler-text err)))))))))

(defun dss/pylint-silence (msgid)
  "Add a special pylint comment to silence a particular warning."
  (interactive (list (read-from-minibuffer "msgid: " (dss/pylint-msgid-at-point))))
  (save-excursion
    (comment-dwim nil)
    (if (looking-at "pylint:")
        (progn (end-of-line)
               (insert ","))
        (insert "pylint: disable-msg="))
    (insert msgid)))

(byte-recompile-directory "~/.emacs.d" 0)
(global-set-key (kbd "C-j") 'avy-goto-char)

;(require 'expand-region)
;(global-set-key (kbd "C-c =") 'er/expand-region)

;; (require 'helm)
;; (require 'helm-config)

;; ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
;; (global-set-key (kbd "C-c h") 'helm-command-prefix)
;; (global-unset-key (kbd "C-x c"))

;; (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
;; (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
;; (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

;; (when (executable-find "curl")
;;   (setq helm-google-suggest-use-curl-p t))

;; (setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
;;       helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
;;       helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
;;       helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
;;       helm-ff-file-name-history-use-recentf t)

;; (helm-mode 1)
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")                                                                                            
(load-theme 'zenburn t)      
