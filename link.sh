if [ -e ~/.bash_aliases ]
then
    rm -f ~/.bash_aliases
fi
ln -f -s `pwd`/.bash_aliases ~/.bash_aliases

if [ -e ~/.bash_host_aliases ]
then
    rm -f ~/.bash_host_aliases
fi
ln -f -s `pwd`/.bash_host_aliases ~/.bash_host_aliases

if [ -e ~/.bashrc ]
then
    rm -f ~/.bashrc
fi
ln -f -s `pwd`/.bashrc ~/.bashrc
# ln -f -s `pwd`/.emacs.d/init.el ~/.emacs

if [ -e ~/.emacs.d ]
then
    rm -f ~/.emacs.d
fi
ln -f -s `pwd`/.emacs.d ~/.emacs.d

if [ -e ~/bin ]
then
    rm -f ~/bin
fi
ln -f -s `pwd`/bin ~/bin

if [ -e ~/.config ]
then
    mv -f ~/.config ~/.config.old
fi
ln -f -s `pwd`/.config ~/.config

if [ -e ~/.profile ]
then
    rm -f ~/.profile
fi
ln -f -s `pwd`/.profile ~/.profile

if [ -e ~/.gitconfig ]
then
    rm -f ~/.gitconfig
fi
ln -f -s `pwd`/.gitconfig ~/.gitconfig


if [ -e ~/.z.sh ]
then
    rm -f ~/.z.sh
fi
ln -f -s `pwd`/z.sh ~/.z.sh

if [ -e ~/.gitignore ]
then
    rm -f ~/.gitignore
fi
ln -f -s `pwd`/.gitignore ~/.gitignore
git config --global core.excludesfile ~/.gitignore
